<?php

require_once __DIR__ . '/vendor/autoload.php';

include(__DIR__ . '/config.php');

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exchange\AMQPExchangeType;
use PhpAmqpLib\Message\AMQPMessage;

$exchange = 'router';
$queue = 'msgs';

$connection = new AMQPStreamConnection(HOST, PORT, USER, PASS, VHOST);
$channel = $connection->channel();

$corr_id = uniqid();
$response = false;

list($callback_queue, ,) = $channel->queue_declare(
    "",
    false,
    false,
    true,
    false
);

$channel->basic_consume(
    $callback_queue,
    '',
    false,
    true,
    false,
    false,
    function ($rep) use ($corr_id, &$response) {
        if ($rep->get('correlation_id') == $corr_id) {
            print('Received response: ' . json_encode($rep->body) . "\n");
            $response = true;
        }
    }
);

/*
    The following code is the same both in the consumer and the producer.
    In this way we are sure we always have a queue to consume from and an
        exchange where to publish messages.
*/

/*
    name: $queue
    passive: false
    durable: true // the queue will survive server restarts
    exclusive: false // the queue can be accessed in other channels
    auto_delete: false //the queue won't be deleted once the channel is closed.
*/
$channel->queue_declare($queue, false, true, false, false);

/*
    name: $exchange
    type: direct
    passive: false
    durable: true // the exchange will survive server restarts
    auto_delete: false //the exchange won't be deleted once the channel is closed.
*/

$channel->exchange_declare($exchange, AMQPExchangeType::DIRECT, false, true, false);

$messageBody = implode(' ', array_slice($argv, 1));
$message = new AMQPMessage($messageBody, [
    'content_type' => 'text/plain',
    'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT,
    'correlation_id' => $corr_id,
    'reply_to' => $callback_queue,
]);
$channel->basic_publish($message, $exchange);

while (!$response) {
    $channel->wait();
}

$channel->close();
$connection->close();

# Rabbit PHP Client demo

See https://github.com/php-amqplib/php-amqplib

## Usage

With RabbitMQ running open two Terminals and on the first one execute the following commands to start the consumer:

    php consumer.php

Then on the other Terminal do:

    php publisher.php some text to publish

You should see the message arriving to the process on the other Terminal

Then to stop the consumer, send to it the quit message:

    php publisher.php quit

<?php
# Find out the host's IP from a docker container with:
# netstat -nr | grep '^0\.0\.0\.0' | awk '{print $2}'
# Alternatively, you can use 172.17.0.1 (docker0 interface ip).
define('HOST', 'localhost');
define('PORT', '5672');
define('USER', 'guest');
define('PASS', 'guest');
define('VHOST', '/');

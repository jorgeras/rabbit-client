<?php
require_once(__DIR__ . '/config.php');

$cnn = new AMQPConnection([
    'host' => HOST,
    'post' => PORT,
    'login' => USER,
    'password' => PASS,
    'vhost' => VHOST,
]);
$cnn->connect();
$channel = new AMQPChannel($cnn);

$exchange = new AMQPExchange($channel);

$rpcServerQueueName = 'rpc_queue';

$srvr_queue = new AMQPQueue($channel);
$srvr_queue->setName($rpcServerQueueName); //intentionally declares the rpc_server queue name
$srvr_queue->declareQueue();

$srvr_queue->consume(function (AMQPEnvelope $message, AMQPQueue $q) use (&$exchange) {
    printf("Received request from %s: %s", $message->getReplyTo(), $message->getBody());

    //publish with the exchange instance to the reply to queue
    $exchange->publish(
        json_encode(['processed by remote service!']),           //reponse message
        $message->getReplyTo(),                                  // get the reply to queue from the message
        AMQP_NOPARAM & AMQP_IMMEDIATE,                      // disable all other params
        [
            'correlation_id' => $message->getCorrelationId()     // obtain and respond with correlation id
        ]
    );

    //acknowledge receipt of the message
    $q->ack($message->getDeliveryTag());
});

/*function shutdown($channel, $connection)
{
    $channel->close();
    $connection->close();
}

register_shutdown_function('shutdown', $channel, $connection);

// Loop as long as the channel has callbacks registered
while ($channel ->is_consuming()) {
    $channel->wait();
}*/

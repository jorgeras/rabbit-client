<?php
require_once(__DIR__ . '/config.php');

$cnn = new AMQPConnection([
    'host' => HOST,
    'post' => PORT,
    'login' => USER,
    'password' => PASS,
    'vhost' => VHOST,
]);
$cnn->connect();
$channel = new AMQPChannel($cnn);

$exchange = new AMQPExchange($channel);

$rpcServerQueueName = 'rpc_queue';

$client_queue = new AMQPQueue($channel);
$client_queue->setFlags(AMQP_EXCLUSIVE);
$client_queue->declareQueue();
$callbackQueueName = $client_queue->getName(); //e.g. amq.gen-JzTY20BRgKO-HjmUJj0wLg

//Set Publish Attributes
$corrId = uniqid();
$attributes = [
    'correlation_id' => $corrId,
    'reply_to' => $callbackQueueName,
];

$exchange->publish(
    json_encode(['request message']),
    $rpcServerQueueName,
    AMQP_NOPARAM,
    $attributes
);

//listen for response
$callback = function (AMQPEnvelope $message, AMQPQueue $q) use ($corrId) {
    if ($message->getCorrelationId() == $corrId) {
        printf("Received message: %s\n", $message->getBody());
        $q->nack($message->getDeliveryTag());

        return false; //return false to signal to consumer that you're done. otherwise it continues to block
    } else {
        printf("Received a message but not for me: %s\n", $message->getCorrelationId());
    }

    return true;
};

$client_queue->consume($callback);


function shutdown($channel, $connection)
{
    $channel->close();
    $connection->disconnect();
}

register_shutdown_function('shutdown', $channel, $cnn);
